INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_OGG_SOURCE ogg_source)

FIND_PATH(
    OGG_SOURCE_INCLUDE_DIRS
    NAMES ogg_source/api.h
    HINTS $ENV{OGG_SOURCE_DIR}/include
        ${PC_OGG_SOURCE_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    OGG_SOURCE_LIBRARIES
    NAMES gnuradio-ogg_source
    HINTS $ENV{OGG_SOURCE_DIR}/lib
        ${PC_OGG_SOURCE_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/ogg_sourceTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(OGG_SOURCE DEFAULT_MSG OGG_SOURCE_LIBRARIES OGG_SOURCE_INCLUDE_DIRS)
MARK_AS_ADVANCED(OGG_SOURCE_LIBRARIES OGG_SOURCE_INCLUDE_DIRS)
