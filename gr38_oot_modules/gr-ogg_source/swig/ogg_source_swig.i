/* -*- c++ -*- */

#define OGG_SOURCE_API

%include "gnuradio.i"           // the common stuff

//load generated python docstrings
%include "ogg_source_swig_doc.i"

%{
#include "ogg_source/ogg_source.h"
%}

%include "ogg_source/ogg_source.h"
GR_SWIG_BLOCK_MAGIC2(ogg_source, ogg_source);
