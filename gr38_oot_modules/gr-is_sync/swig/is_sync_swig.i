/* -*- c++ -*- */

#define IS_SYNC_API

%include "gnuradio.i"           // the common stuff

//load generated python docstrings
%include "is_sync_swig_doc.i"

%{
#include "is_sync/is_sync_a.h"
%}

%include "is_sync/is_sync_a.h"
GR_SWIG_BLOCK_MAGIC2(is_sync, is_sync_a);
