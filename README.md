# satnogs-apt-ber-tools

Utilities to derive test data (SNR and bit error rate) from NOAA APT observations recorded via SatNOGS.

Note that this is still a highly experimental **work in progress**.

## Requirements

* Python 3 (tested with 3.7+, parts might work with older versions as well)
* GNU Radio 3.8+
* GNU Make
* CMake
* SWIG (via GNU Radio)
* Boost (via GNU Radio)
* python-virtualenv
* Python dependencies: NumPy, SciPy, Requests, Joblib, python-dateutil, tqdm, PyEphem

## Getting Started

A `Makefile` preparing a virtual environment is provided for your convenience. However, it has only been tested with Arch Linux.

First, install a recent version of Python 3, CMake, GNU Radio 3.8+, SWIG, Boost, and the GNU Radio Python extension on your system. The components have to be found by CMake.

Recommended command for Arch Linux:

```
pacman -Sy base-devel cmake gnuradio python python-virtualenv numpy scipy boost boost-libs swig
```

Afterwards, run:

```
make virtualenv
```

This command will create a new virtual environment in `.venv`, build and install required GNU Radio OOT modules inside the environment, and install necessary Python dependencies.

Now you can fetch observations and analyze them as follows:

```
source .venv/bin/activate
python fetch_satnogs_observations.py -o observations.json -O <observation IDs>
python analyze_satnogs_observations.py -o observations.pick observations.json
```

Decode and show a specific APT observation as follows:

```
python show_synced_apt_image_for_observation.py <observation ID>
```

The process has not been tested and probably will not work with the PyBOMBS installation as recommended by GNU Radio developers. Contributions enabling support for it would be highly appreciated!

## Acknowledgment

The majority of code in `gr38_oot_modules` has been adapted from [gr-satnogs](https://gitlab.com/librespacefoundation/satnogs/gr-satnogs) to work with Python 3 and GNU Radio 3.8. As support is now present in gr-satnogs (v2.0) itself, a replacement of the custom adaptations is pending.

The APT decoder (noaa_apt_snr_from_ogg.py) is based on the original APT decoder provided in gr-satnogs plus contributions by Alexander Jenke.

## License

This code is licensed under GPLv3. See [LICENSE](LICENSE) for details.
