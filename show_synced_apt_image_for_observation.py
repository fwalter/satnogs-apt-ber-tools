#!/usr/bin/env python3
# encoding: utf-8

import argparse
import os

import json
import numpy as np
import matplotlib
import matplotlib.dates

matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt

from satnogs_apt_ber_tools import (
    satnogs_api,
    signal_analysis,
)

SATNOGS_NETWORKS = {
    "dev": satnogs_api.DEV_NETWORK_URL,
    "prod": satnogs_api.NETWORK_URL,
}


def _main(args):
    data = None
    if args.file:
        filedata = json.load(args.file)
        downloadpath = filedata["download_path"]
        observations = filedata["observations"]
        for gsid, satid, obs_tuples in observations:
            for obs, obsdata in obs_tuples:
                if obs["id"] == args.OBSERVATION:
                    data = obsdata
                    break
            if data:
                break
    elif not args.downloadpath:
        downloadpath = os.curdir
    data = data or satnogs_api.fetch_observation_data(
        args.OBSERVATION,
        network_url=SATNOGS_NETWORKS[args.network],
        download_path=(args.downloadpath or downloadpath),
    )
    print("Received data: ")
    print(data)
    if not os.path.isfile(data["file"]):
        print("Warning: Audio data missing at " + data["file"])
    if data["vetted_status"] != "good":
        print("Warning: Using contact which was not vetted as 'good'")

    unsynced, synced, has_sync, off, corr = signal_analysis.decode_apt_image(
        ogg_file=data["file"],
        dest_path=(args.downloadpath or downloadpath),
        force=args.reanalyze,
    )

    print(
        "Seconds in image: ",
        len(synced) / 2,
        "- seconds in contact: ",
        data["end_unix"] - data["start_unix"],
    )

    if args.clip:
        synced = np.clip(synced, 0, 1)

    plt.imshow(synced, interpolation="nearest", cmap=args.colorscheme)
    plt.tight_layout()

    if args.storedata:
        json.dump(data, args.storedata)
        args.storedata.close()
    if args.storeimg:
        plt.savefig(args.storeimg)

    plt.show()


def _get_argument_parser():
    parser = argparse.ArgumentParser(
        description="view the decoded APT image for a SatNOGS observation",
    )
    parser.add_argument(
        "OBSERVATION",
        type=int,
        help="the observation ID to be decoded",
    )
    parser.add_argument(
        "-f", "--file",
        type=argparse.FileType("r"),
        default=None,
        help="use input file containing pre-fetched SatNOGS data",
    )
    parser.add_argument(
        "-n", "--network",
        choices=SATNOGS_NETWORKS.keys(),
        default="prod",
        help="the SatNOGS network to be used",
    )
    parser.add_argument(
        "-s", "--colorscheme",
        default="gray",
        help="override the used colorscheme",
    )
    parser.add_argument(
        "-c", "--clip",
        action="store_true",
        help="clip to [0; 1] value range",
    )
    parser.add_argument(
        "-r", "--reanalyze",
        action="store_true",
        help="force reanalysis of audio file",
    )
    parser.add_argument(
        "--downloadpath",
        default=None,
        help="the path for downloading received data",
    )
    parser.add_argument(
        "--storedata",
        type=argparse.FileType("w"),
        default=None,
        help="store fetched data to the given file",
    )
    parser.add_argument(
        "--storeimg",
        type=str, default=None,
        help="store decoded image to the given file",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
