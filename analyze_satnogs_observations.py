#!/usr/bin/env python3
# encoding: utf-8

"""
Tool to analyze a set of APT observations from SatNOGS network regarding SNR
and error rates and store the results as a Joblib Pickle file.
"""

import sys
import argparse
import json
import numpy as np
import joblib
import tqdm

from satnogs_apt_ber_tools.signal_analysis import (
    DEFAULT_SAMPLE_RATE,
    decode_apt_image,
    determine_snrs,
)


def process_observation(gsid, satid, obs, data, downloadpath, maxmismatch,
                        forcereanalysis):
    log = ["Processing observation #{} ({} <-> {} @ [{}; {}])".format(
        obs["id"],
        data["station_name"],
        data["satellite_info"]["name"],
        data["start"],
        data["end"],
    )]
    data["analyzed"] = False
    try:
        snrs = determine_snrs(
            data["file"],
            downloadpath,
            forcereanalysis,
        )
    except Exception as e:  # pylint: disable=broad-except
        log.append(
            "ERROR: Could not determine SNR: {}; skipping".format(e)
        )
        return data, log
    expected_value_count = (
        DEFAULT_SAMPLE_RATE *
        (data["end_unix"] - data["start_unix"])
    )
    log.append("SNR estimator returned {} value(s), expected {}".format(
        len(snrs),
        expected_value_count,
    ))
    if abs(expected_value_count - len(snrs)) > 10:
        log.append("WARN: Mismatch (expected - measured) = {} > 10".format(
            expected_value_count - len(snrs),
        ))
    if abs(expected_value_count - len(snrs)) > maxmismatch:
        log.append("ERROR: Mismatch > {} (broken ogg file?); skipping".format(
            maxmismatch,
        ))
        return data, log
    log.append("Processing APT data...")
    try:
        unsynced, synced, has_sync, off, corr = decode_apt_image(
            data["file"],
            downloadpath,
            forcereanalysis,
        )
        linedata = np.array([
            np.concatenate((line[0:90], line[1040:1130]))
            for line in synced
        ])
    except Exception as e:  # pylint: disable=broad-except
        log.append(
            "ERROR: Could not determine synced img: {}; skipping".format(e)
        )
        return data, log
    data.update({
        "measured_snrs": snrs,
        "linedata": linedata,
        "has_sync": has_sync,
        "correlation_values": corr,
        "analyzed": True,
    })
    log.append("DONE processing observation #{}".format(
        obs["id"],
    ))
    return data, log


def _main(args):
    data = json.load(args.FILE)
    obs_tuples = data["observations"]
    downloadpath = args.downloadpath or data["download_path"]
    res = []
    observations = []
    for entry in obs_tuples:
        gsid, satid, obs_list = tuple(entry)
        if args.count:
            obs_list = obs_list[:args.count]
        for obs, data in obs_list:
            observations.append((gsid, satid, obs, data))
    print("Processing {} observations using {} worker(s)...".format(
        len(observations),
        args.workers,
    ), file=sys.stderr)
    res = joblib.Parallel(n_jobs=args.workers)(
        joblib.delayed(process_observation)(
            gsid,
            satid,
            obs,
            data,
            downloadpath,
            args.maxmismatch,
            args.forcereanalysis,
        )
        for gsid, satid, obs, data in tqdm.tqdm(observations)
    )
    print("Dumping output via joblib...", file=sys.stderr)
    joblib.dump([data for data, _ in res], args.outfile)
    print("Dumping logfile...", file=sys.stderr)
    args.logfile.write("\n".join(
        (e[:3000] + "..." if len(e) > 3000 else e)
        for _, log in res
        for e in log
    ) + "\n")
    args.logfile.close()
    ok_count = sum(1 for data, _ in res if data["analyzed"])
    print("{} of {} observations OK ({} %)".format(
        ok_count,
        len(observations),
        round(ok_count / len(observations) * 100, 2),
    ), file=sys.stderr)


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "FILE",
        type=argparse.FileType("r"), default=sys.stdin,
        help="the input file as written by fetch_satnogs_observations.py",
    )
    parser.add_argument(
        "--maxmismatch",
        type=int, default=30,
        help="the maximum difference between predicted and analyzed duration",
    )
    parser.add_argument(
        "-c", "--count",
        type=int, default=None,
        help="the maximum count of observations to be analyzed",
    )
    parser.add_argument(
        "-o", "--outfile",
        required=True,
        help="the output filename",
    )
    parser.add_argument(
        "-l", "--logfile",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the logfile",
    )
    parser.add_argument(
        "-w", "--workers",
        type=int, default=4,
        help="the amount of workers used for parallel processing",
    )
    parser.add_argument(
        "--downloadpath",
        default=None,
        help="override the path containing downloaded data",
    )
    parser.add_argument(
        "-f", "--forcereanalysis",
        action="store_true",
        help="force data re-analysis",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
