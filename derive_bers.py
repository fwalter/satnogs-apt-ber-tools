#!/usr/bin/env python3
# encoding: utf-8

import sys
import argparse
import datetime
import collections

import joblib
import ephem
import numpy as np
import scipy.special
import tqdm

TIME_RESOLUTION = 1
APT_RESOLUTION = 0.5

LINK_PARAMETERS = {
    # Widespread RS(255, 223) code as used by Voyager probes
    "255rs223": (223 / 255, 255, 16),
    # https://public.ccsds.org/Pubs/231x1o1.pdf
    # see also: http://pretty-good-codes.org/ldpc.html
    "ccsds256ldpc128": (128 / 256, 256, 24),
    "ccsds128ldpc64": (64 / 128, 128, 14),
    "nofail": (1, 1, 1),
    "none": (1, 1, 0),
}

ContactBERInfo = collections.namedtuple("ContactBERInfo", [
    "station",
    "satellite",
    "starttime",
    "endtime",
    "max_elevation",
    "timestamps",
    "line_bers_down",
    "line_bers_up",
])


def _ber_after_coding(ber_before_coding, uncoded_bit_rate,
                      fec_block_size, fec_correctable_bits):
    p_successful_block_transfer = 0
    # add up the probabilities for successful decoding
    for i in range(fec_correctable_bits + 1):
        p_successful_block_transfer += (
            # `block size - i` bits are correct
            (1 - ber_before_coding) ** (fec_block_size - i) *
            # i bits are wrong
            ber_before_coding ** i *
            # count of combinations possible with i wrong bits
            # as we can correct arbitrary combinations
            scipy.special.comb(fec_block_size, i)
        )
    # Get a BER back
    return 1 - (p_successful_block_transfer ** (1 / fec_block_size))


def _get_doi(line_data):
    doi = []
    for i, line in enumerate(line_data):
        first = line[44:86]
        second = line[134:176]
        # check if line is inverted (in case we encountered an IR image or
        # the minute marker...)
        if first.mean() < .5:
            first = 1 - first
        if second.mean() < .5:
            second = 1 - second
        # add combined line to doi
        doi.append(np.concatenate((first, second)))
        assert len(doi[-1] == 84)
    return np.array(doi, ndmin=2, dtype=float)


def _resample_image(image, lines_to_combine):
    new_image = [
        image[i * lines_to_combine:(i + 1) * lines_to_combine]
        for i in range(len(image) // lines_to_combine)
    ]
    return np.array(new_image, ndmin=2, dtype=float)


def _estimate_apt_space_pixel_ber(pixels):

    # estimation kernel == gauss kernel
    def q(value):
        return .5 * scipy.special.erfc(value / np.sqrt(2))

    # rescale such that a transmitted 0 maps to -1 and 1 to 1
    in_data = (pixels * 2 - 1).flatten()
    # bandwidth, optimized for gauss kernel
    h_n = (3 / (4 * len(in_data))) ** (1 / 5) * np.std(in_data)

    # we have only one value and, thus, need to assume the BER is symmetric...
    # (uplink / downlink)
    p_err = 1 / len(in_data) * sum(q(x / h_n) for x in in_data)

    return p_err


def _preprocess_contact_dict(contact_dict, soft_start, soft_end):
    timestamps = np.arange(soft_start, soft_end, APT_RESOLUTION)
    dates = [
        datetime.datetime.utcfromtimestamp(ts)
        for ts in timestamps
    ]

    img_part = contact_dict["linedata"]
    has_sync = contact_dict["has_sync"]

    assert abs(
        len(img_part) * APT_RESOLUTION -
        len(contact_dict["measured_snrs"])
    ) < 10  # 10 seconds
    assert len(img_part) < len(dates)
    assert abs(len(img_part) - len(dates)) < 30 / APT_RESOLUTION  # 30 seconds
    assert len(img_part) == len(has_sync)

    # align dates to measurements
    # NOTE: the >= 2 sec SNR setup time is main factor, so we remove at start
    new_start = len(dates) - len(img_part)
    dates = dates[new_start:]
    timestamps = timestamps[new_start:]

    return img_part, has_sync, dates, timestamps


def _get_sat_distance_light_sec(sat_name, tle_array,
                                station_lat, station_lon, station_alt_m,
                                unixtime):
    gsobj = ephem.Observer()
    gsobj.lat = str(station_lat)
    gsobj.lon = str(station_lon)
    gsobj.date = datetime.datetime.utcfromtimestamp(unixtime)
    satobj = ephem.readtle(sat_name, *tle_array)
    satobj.compute(gsobj)
    return satobj.range / 299_792_458


def _determine_bers(contact_dict, bergranularity, fec_block_size,
                    fec_correctable_b, overall_bit_rate_down,
                    overall_bit_rate_up, resampling_factor):
    cid = contact_dict["id"]
    station = contact_dict["station_name"]
    satellite = contact_dict["satellite_info"]["name"]
    soft_start = contact_dict["start_unix"]
    soft_end = contact_dict["end_unix"]
    max_elevation = contact_dict["max_altitude"]

    try:
        img_part, has_sync, dates, timestamps = _preprocess_contact_dict(
            contact_dict,
            soft_start,
            soft_end,
        )
    except AssertionError as e:
        print(f"Contact #{cid} {station} <-> {satellite}, "
              f"interval [{soft_start}; {soft_end}] "
              f"unusable, assertion failed: {e}", file=sys.stderr)
        return None
    except KeyError:
        print(f"Contact #{cid} {station} <-> {satellite}, "
              f"interval [{soft_start}; {soft_end}] "
              f"unusable, was not analyzed properly!", file=sys.stderr)
        return None

    resampled_doi = _resample_image(
        _get_doi(img_part),
        resampling_factor,
    )
    # Clip the timestamps array to match the resampled lines
    timestamps = timestamps[0:(len(resampled_doi) * resampling_factor)]
    line_bers_down = [
        _ber_after_coding(
            _estimate_apt_space_pixel_ber(line),
            overall_bit_rate_down,
            fec_block_size,
            fec_correctable_b,
        )
        for line in resampled_doi
    ]
    line_bers_up = [
        _ber_after_coding(
            _estimate_apt_space_pixel_ber(line),
            overall_bit_rate_up,
            fec_block_size,
            fec_correctable_b,
        )
        for line in resampled_doi
    ]

    starttime = timestamps[0]
    endtime = timestamps[-1] + bergranularity

    return ContactBERInfo(
        station=station,
        satellite=satellite,
        starttime=starttime,
        endtime=endtime,
        max_elevation=max_elevation,
        timestamps=timestamps,
        line_bers_down=line_bers_down,
        line_bers_up=line_bers_up,
    )


def _main(args):
    print("Unpickling input data...", file=sys.stderr)
    data = joblib.load(args.INFILE)

    fec_rate, fec_block_size, fec_correctable_b = LINK_PARAMETERS[args.fec]
    overall_bit_rate_down = args.bitrate_downlink
    overall_bit_rate_up = (
        args.bitrate_uplink if args.bitrate_uplink else overall_bit_rate_down
    )
    # net_bit_rate_down = overall_bit_rate_down * fec_rate
    # net_bit_rate_up = overall_bit_rate_up * fec_rate

    # use e.g. 20 lines per step if bergranularity == 10 seconds
    resampling_factor = int(args.bergranularity / APT_RESOLUTION)

    # Second, generate the F-TVG
    print("Processing contact plans and determining BER...", file=sys.stderr)
    ftup = joblib.Parallel(n_jobs=args.workers)(
        joblib.delayed(_determine_bers)(
            contact_dict,
            bergranularity=args.bergranularity,
            fec_block_size=fec_block_size,
            fec_correctable_b=fec_correctable_b,
            overall_bit_rate_down=overall_bit_rate_down,
            overall_bit_rate_up=overall_bit_rate_up,
            resampling_factor=resampling_factor,
        )
        for contact_dict in tqdm.tqdm(data)
    )

    print("Writing BER data to file...", file=sys.stderr)
    joblib.dump(ftup, args.outfile)


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "INFILE",
        help="the input filename, generated by APT analysis",
    )
    parser.add_argument(
        "-o", "--outfile",
        required=True,
        help="the output filename",
    )
    parser.add_argument(
        "-r", "--bitrate-downlink",
        type=float, default=4160,
        help="the assumed downlink bit rate (default: 4.16 kbit/s)",
    )
    parser.add_argument(
        "-R", "--bitrate-uplink",
        type=float, default=None,
        help="the assumed uplink bit rate (default: same as downlink)",
    )
    parser.add_argument(
        "-g", "--bergranularity",
        type=int, default=10,
        help="the assumed granularity of BER windows in s (default: 10 s)",
    )
    parser.add_argument(
        "--fec",
        choices=LINK_PARAMETERS.keys(), default="255rs223",
        help="the used FEC scheme (default: Voyager RS(255,223)",
    )
    parser.add_argument(
        "-w", "--workers",
        type=int, default=4,
        help="the amount of workers used for parallel processing",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
