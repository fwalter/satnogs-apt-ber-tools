#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Noaa Apt Snr From Ogg
# Author: Alexander Jenke and Felix Walter, based on the SatNOGS project
# GNU Radio version: 3.8.0.0

from gnuradio import blocks
from gnuradio import filter
from gnuradio.filter import firdes
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import is_sync
import ogg_source

class noaa_apt_snr_from_ogg(gr.top_block):

    def __init__(self, input_file_path="source.ogg", input_sample_rate=48000, output_raw_file_path="sync_a.out", output_sync_a_file_path="raw.out"):
        gr.top_block.__init__(self, "Noaa Apt Snr From Ogg")

        ##################################################
        # Parameters
        ##################################################
        self.input_file_path = input_file_path
        self.input_sample_rate = input_sample_rate
        self.output_raw_file_path = output_raw_file_path
        self.output_sync_a_file_path = output_sync_a_file_path

        ##################################################
        # Blocks
        ##################################################
        self.rational_resampler_xxx_0_1 = filter.rational_resampler_fff(
                interpolation=4*4160,
                decimation=input_sample_rate,
                taps=None,
                fractional_bw=None)
        self.rational_resampler_xxx_0_0 = filter.rational_resampler_fff(
                interpolation=1,
                decimation=4,
                taps=None,
                fractional_bw=None)
        self.ogg_source_ogg_source_0 = ogg_source.ogg_source(input_file_path, 1, False)
        self.is_sync_is_sync_a_0 = is_sync.is_sync_a()
        self.hilbert_fc_0 = filter.hilbert_fc(65, firdes.WIN_HAMMING, 6.76)
        self.blocks_file_sink_2 = blocks.file_sink(gr.sizeof_float*1, output_raw_file_path, False)
        self.blocks_file_sink_2.set_unbuffered(False)
        self.blocks_file_sink_1 = blocks.file_sink(gr.sizeof_int*1, output_sync_a_file_path, False)
        self.blocks_file_sink_1.set_unbuffered(False)
        self.blocks_complex_to_mag_0 = blocks.complex_to_mag(1)
        self.band_pass_filter_0 = filter.fir_filter_fff(
            1,
            firdes.band_pass(
                6,
                input_sample_rate,
                1,
                4.2e3,
                100,
                firdes.WIN_HAMMING,
                6.76))



        ##################################################
        # Connections
        ##################################################
        self.connect((self.band_pass_filter_0, 0), (self.rational_resampler_xxx_0_1, 0))
        self.connect((self.blocks_complex_to_mag_0, 0), (self.rational_resampler_xxx_0_0, 0))
        self.connect((self.hilbert_fc_0, 0), (self.blocks_complex_to_mag_0, 0))
        self.connect((self.is_sync_is_sync_a_0, 0), (self.blocks_file_sink_1, 0))
        self.connect((self.ogg_source_ogg_source_0, 0), (self.band_pass_filter_0, 0))
        self.connect((self.rational_resampler_xxx_0_0, 0), (self.blocks_file_sink_2, 0))
        self.connect((self.rational_resampler_xxx_0_0, 0), (self.is_sync_is_sync_a_0, 0))
        self.connect((self.rational_resampler_xxx_0_1, 0), (self.hilbert_fc_0, 0))

    def get_input_file_path(self):
        return self.input_file_path

    def set_input_file_path(self, input_file_path):
        self.input_file_path = input_file_path

    def get_input_sample_rate(self):
        return self.input_sample_rate

    def set_input_sample_rate(self, input_sample_rate):
        self.input_sample_rate = input_sample_rate
        self.band_pass_filter_0.set_taps(firdes.band_pass(6, self.input_sample_rate, 1, 4.2e3, 100, firdes.WIN_HAMMING, 6.76))

    def get_output_raw_file_path(self):
        return self.output_raw_file_path

    def set_output_raw_file_path(self, output_raw_file_path):
        self.output_raw_file_path = output_raw_file_path
        self.blocks_file_sink_2.open(self.output_raw_file_path)

    def get_output_sync_a_file_path(self):
        return self.output_sync_a_file_path

    def set_output_sync_a_file_path(self, output_sync_a_file_path):
        self.output_sync_a_file_path = output_sync_a_file_path
        self.blocks_file_sink_1.open(self.output_sync_a_file_path)


def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "--input-sample-rate", dest="input_sample_rate", type=intx, default=48000,
        help="Set input_sample_rate [default=%(default)r]")
    return parser


def main(top_block_cls=noaa_apt_snr_from_ogg, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(input_sample_rate=options.input_sample_rate)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
