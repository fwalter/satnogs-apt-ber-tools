#!/usr/bin/env python3
# encoding: utf-8

"""
Tool to fetch a set of observations from SatNOGS network.
"""

import os
import sys
import argparse
import json
import time
import logging

import requests

from satnogs_apt_ber_tools import satnogs_api

SATNOGS_NETWORKS = {
    "dev": satnogs_api.DEV_NETWORK_URL,
    "prod": satnogs_api.NETWORK_URL,
}

RETRIES = 5
RETRY_TIMEOUT = 5


def _fetch_observation(oid, logger, network_url, downloadpath):
    data = None
    logger.info("Fetching observation {}".format(oid))
    for retry in range(RETRIES):
        try:
            data = satnogs_api.fetch_observation_data(
                oid,
                network_url=network_url,
                download_path=downloadpath,
            )
            break
        except requests.exceptions.RequestException as e:
            logger.warning("Requests Exception: {}, retrying in {}s...".format(
                e,
                RETRY_TIMEOUT,
            ))
            time.sleep(RETRY_TIMEOUT)
            continue
    if data is None:
        logger.fatal("Error fetching observation data, terminating.")
        sys.exit(1)
    return data


def _fetch_sat_gs_pair(gsid, satid, obs_count, start, end,
                       network_url, downloadpath, logger):
    observations = None
    for retry in range(RETRIES):
        try:
            observations = satnogs_api.fetch_observations(
                gsid,
                satid,
                obs_count,
                date_range=((start, end) if start and end else None),
                network_url=network_url,
            )
            break
        except requests.exceptions.RequestException as e:
            logger.warning("Requests Exception: {}, retrying in {}s...".format(
                e,
                RETRY_TIMEOUT,
            ))
            time.sleep(RETRY_TIMEOUT)
            continue
    if observations is None:
        logger.fatal("Error fetching observations, terminating.")
        sys.exit(1)
    logger.info("{} observations found: {}".format(
        len(observations),
        [o["id"] for o in observations],
    ))
    obs_pairs = []
    for obs in sorted(observations, key=lambda x: x["id"]):
        data = _fetch_observation(
            obs["id"],
            logger=logger,
            network_url=network_url,
            downloadpath=downloadpath,
        )
        obs_pairs.append((obs, data))
    return obs_pairs


def _main(args):
    logger = _initialize_logger(args.verbose)
    if (args.gs is None or args.sat is None) and args.observations is None:
        logger.fatal("Error: Either sat AND gs, OR observations has to be set")
        sys.exit(1)
    if args.observations:
        obs_dict = {}
        for oid in args.observations:
            obs = satnogs_api.fetch_observation_info(
                oid,
                network_url=SATNOGS_NETWORKS[args.network],
            )
            sat_id = obs["norad_cat_id"]
            gs_id = obs["ground_station"]
            data = _fetch_observation(
                obs["id"],
                logger=logger,
                network_url=SATNOGS_NETWORKS[args.network],
                downloadpath=args.downloadpath,
            )
            if (gs_id, sat_id) not in obs_dict:
                obs_dict[(gs_id, sat_id)] = []
            obs_dict[(gs_id, sat_id)].append((obs, data))
        obs_tuples = [
            (gs_id, sat_id, obs_list)
            for (gs_id, sat_id), obs_list in obs_dict.items()
        ]
    else:
        obs_tuples = []
        for gsid in args.gs:
            for satid in args.sat:
                logger.info(
                    ">>> Fetching observations for gs = {}, sat = {}".format(
                        gsid,
                        satid,
                    )
                )
                obs_list = _fetch_sat_gs_pair(
                    gsid,
                    satid,
                    args.count,
                    args.start,
                    args.end,
                    network_url=SATNOGS_NETWORKS[args.network],
                    downloadpath=args.downloadpath,
                    logger=logger,
                )
                obs_tuples.append((gsid, satid, obs_list))
    data = {
        "observations": obs_tuples,
        "download_path": args.downloadpath,
    }
    json.dump(data, args.outfile, indent=(4 if args.indent else None))
    args.outfile.write("\n")
    args.outfile.close()


def _initialize_logger(verbosity):
    logging.basicConfig(
        level={
            0: logging.INFO,
        }.get(verbosity, logging.DEBUG),
        format="%(asctime)-23s %(levelname)s - %(name)s: %(message)s",
    )
    return logging.getLogger(sys.argv[0])


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-g", "--gs",
        type=int, default=None,
        nargs="*",
        help="the GS(s) to be used",
    )
    parser.add_argument(
        "-s", "--sat",
        type=int, default=None,
        nargs="*",
        help="the satellite(s) to be used",
    )
    parser.add_argument(
        "-0", "--start",
        default=None,
        help="the start date in ISO format",
    )
    parser.add_argument(
        "-1", "--end",
        default=None,
        help="the start date in ISO format",
    )
    parser.add_argument(
        "-c", "--count",
        type=int, default=None,
        help="the maximum count of observations to be fetched (default: all)",
    )
    parser.add_argument(
        "-O", "--observations",
        type=int, nargs="*", default=None,
        help="specific observations to be fetched (overrides anything else)",
    )
    parser.add_argument(
        "-o", "--outfile",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the output filename",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="indent the JSON output",
    )
    parser.add_argument(
        "-n", "--network",
        choices=SATNOGS_NETWORKS.keys(),
        default="prod",
        help="the SatNOGS network to be used",
    )
    parser.add_argument(
        "--downloadpath",
        default=os.curdir,
        help="the path for downloading received data",
    )
    parser.add_argument(
        "-v", "--verbose",
        action="count",
        default=0,
        help="increase output verbosity",
    )
    return parser


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
