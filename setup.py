#!/usr/bin/env python3
# encoding: utf-8

"""Setup script for the satnogs_apt_ber_tools module."""

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt", "r") as fh:
    requirements = [line.strip() for line in fh.readlines()]

setuptools.setup(
    name="satnogs_apt_ber_tools",
    version="0.1.0",
    author="Felix Walter",
    author_email="code@felix-walter.eu",
    description=(
        "Utilities to generate test data from SatNOGS APT observations."
    ),
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/fwalter/satnogs-tools",
    packages=setuptools.find_packages(),
    install_requires=requirements,
    python_requires=">=3.7",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: POSIX :: Linux",
        "Development Status :: 3 - Alpha",
    ],
)
